package tsd.techscidynamics.com.tsd.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import tsd.techscidynamics.com.tsd.R;
import tsd.techscidynamics.com.tsd.adapter.ListAdapter;
import tsd.techscidynamics.com.tsd.model.Tsd;
import tsd.techscidynamics.com.tsd.utils.TsdUtil;

public class MainActivity extends AppCompatActivity {
    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        final ArrayList<Tsd> tsdList = TsdUtil.getModel(this);

        ListAdapter listAdapter = new ListAdapter(this, tsdList);
        mListView.setAdapter(listAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, PagerActivity.class);
                intent.putExtra("position", position);
                intent.putParcelableArrayListExtra("tsdList", tsdList);
                startActivity(intent);
            }
        });
    }

    private void init() {
        mListView = (ListView) findViewById(R.id.tsd_list);
    }
}
