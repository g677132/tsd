package tsd.techscidynamics.com.tsd.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ggriogoryan on 8/29/16.
 */
public class Tsd implements Parcelable {
    String title;
    String description;

    public Tsd(String title, String description) {
        this.title = title;
        this.description = description;
    }

    protected Tsd(Parcel in) {
        title = in.readString();
        description = in.readString();
    }

    public static final Creator<Tsd> CREATOR = new Creator<Tsd>() {
        @Override
        public Tsd createFromParcel(Parcel in) {
            return new Tsd(in);
        }

        @Override
        public Tsd[] newArray(int size) {
            return new Tsd[size];
        }
    };

    public String getTitle() {
        return title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(description);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}