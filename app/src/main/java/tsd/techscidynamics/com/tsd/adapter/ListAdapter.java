package tsd.techscidynamics.com.tsd.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import tsd.techscidynamics.com.tsd.R;
import tsd.techscidynamics.com.tsd.model.Tsd;

/**
 * Created by ggriogoryan on 8/29/16.
 */
public class ListAdapter extends BaseAdapter {
    private List<Tsd> mTsds;
    private LayoutInflater mInflater;

    public ListAdapter(Context context, List<Tsd> tsds) {
        this.mTsds = tsds;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mTsds.size();
    }

    @Override
    public Object getItem(int position) {
        return mTsds.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.tsd_item, parent, false);
        }

        Tsd tsd = (Tsd) getItem(position);
        TextView title = (TextView) view.findViewById(R.id.tsd_title);
        TextView desc = (TextView) view.findViewById(R.id.tsd_desc);
        desc.setMaxLines(1);
        desc.setEllipsize(TextUtils.TruncateAt.END);
        title.setText(tsd.getTitle());
        desc.setText(tsd.getDescription());
        return view;
    }
}