package tsd.techscidynamics.com.tsd.utils;

import android.content.Context;

import java.util.ArrayList;

import tsd.techscidynamics.com.tsd.R;
import tsd.techscidynamics.com.tsd.model.Tsd;

/**
 * Created by ggriogoryan on 8/29/16.
 */
public class TsdUtil {

    public static ArrayList<Tsd> getModel(Context context) {
        ArrayList<Tsd> tsdList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            tsdList.add(new Tsd(context.getString(R.string.tsd_title) + i, context.getString(R.string.tsd_desc)));
        }
        return tsdList;
    }
}