package tsd.techscidynamics.com.tsd.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import tsd.techscidynamics.com.tsd.R;
import tsd.techscidynamics.com.tsd.model.Tsd;

/**
 * Created by ggriogoryan on 8/29/16.
 */
public class PagerAdapter extends android.support.v4.view.PagerAdapter {
    private Context mContext;
    private List<Tsd> pages;

    public PagerAdapter(Context context, List<Tsd> pages) {
        this.mContext = context;
        this.pages = pages;
    }

    @Override
    public int getCount() {
        return pages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Tsd tsd = pages.get(position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.tsd_item, container, false);
        TextView title = (TextView) layout.findViewById(R.id.tsd_title);
        TextView desc = (TextView) layout.findViewById(R.id.tsd_desc);
        title.setText(tsd.getTitle());
        desc.setText(tsd.getDescription());
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}