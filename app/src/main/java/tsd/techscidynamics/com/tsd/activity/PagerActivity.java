package tsd.techscidynamics.com.tsd.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import tsd.techscidynamics.com.tsd.R;
import tsd.techscidynamics.com.tsd.adapter.PagerAdapter;
import tsd.techscidynamics.com.tsd.model.Tsd;

public class PagerActivity extends AppCompatActivity {
    private ViewPager tsdPager;
    private ImageButton shareButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);
        init();

        ArrayList<Tsd> tsdList = getIntent().getParcelableArrayListExtra("tsdList");
        int position = getIntent().getIntExtra("position", 0);
        PagerAdapter pagerAdapter = new PagerAdapter(this, tsdList);
        tsdPager.setAdapter(pagerAdapter);
        tsdPager.setCurrentItem(position);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                TextView title = (TextView) v.getRootView().findViewById(R.id.tsd_title);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, title.getText());
                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.tsd_share)));
            }
        });
    }

    private void init() {
        tsdPager = (ViewPager) findViewById(R.id.tsd_pager);
        shareButton = (ImageButton) findViewById(R.id.shareButton);
    }
}
